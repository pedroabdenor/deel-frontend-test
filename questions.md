## 1. What is the difference between Component and PureComponent? give an example where it might break my app.

Difference between Component and PureComponent: PureComponent implements shouldComponentUpdate() method that does a shallow comparison of new and previous props and state, while Component re-renders even if props or state have the same value. Context + shouldComponentUpdate() can be dangerous if Context is used to pass information from parent to child and any change in Context triggers an update in all descendent components.

## 2. Context + ShouldComponentUpdate might be dangerous. Can think of why is that?

Using shouldComponentUpdate() with Context can be dangerous because any change in the Context triggers an update in all descendant components, even if their props have not changed. If a component uses shouldComponentUpdate() to avoid unnecessary updates and the value of Context is updated, the component will not update even though the data in Context has changed.

## 3. Describe 3 ways to pass information from a component to its PARENT.

Props: Parent passes data/method to child component.
Context: Child component updates context, and parent reads updated value.
Callback Functions: Parent defines a function and passes it to child to call with data as argument.

## 4. Give 2 ways to prevent components from re-rendering.

Using shouldComponentUpdate() method in your component and make sure it returns false if you don't want the component to re-render.
Using React.memo(), it checks for shallow equality of props and will only re-render the component if the props have changed.

## 5. What is a fragment and why do we need it? Give an example where it might break my app.

Fragment is a component that groups a list of children elements without adding an extra node to the DOM. It is needed to avoid adding unnecessary extra markup to the DOM.

## 6. Give 3 examples of the HOC pattern.


## 7. what's the difference in handling exceptions in promises, callbacks and async...await.

Promises have a built-in .catch() method for handling exceptions, while with callbacks and Promises you can use a try...catch block.

## 8. How many arguments does setState take and why is it async.

setState takes two arguments, an updater function and an optional callback function. It is asynchronous and allows the browser to remain responsive during heavy operations.


## 9. List the steps needed to migrate a Class to Function Component.

Identify the component's state and props.
Remove the render() method and instead use a return statement to return the JSX.
Convert the class constructor to a function that takes props as its parameter.
Replace this.state with useState() hook to manage state.
Remove lifecycle methods and replace them with appropriate hooks.
Replace this.props with props in the functional component.
Remove the class declaration and export the function component instead.

## 10. List a few ways styles can be used with components.

I don't know if I understood the question correctly, but using Inline styles, CSS modules and Styled components.

## 11. How to render an HTML string coming from the server.
dangerouslySetInnerHTML can be used to render HTML content as a string to the DOM.






