import { ArtworksItem } from '../types/ArtworksItem'

interface ArtworksListAPI {
  id: number;
  title: string;
}

export async function getArtworksItem(id: number): Promise<ArtworksItem> {
  try {
    const response = await fetch(`https://api.artic.edu/api/v1/artworks/${id}`);
    const json = await response.json();

    if(json && json.data) {
      return {
        title: json.data.title,
        image_id: json.data.image_id
      }
    } else {
      return {
        title: '',
        image_id: ''
      }
    }

  } catch (e) {
    throw(e);
  }
}

export async function getArtworksList(term: string): Promise<[]> {
  try {
    const response = await fetch(`https://api.artic.edu/api/v1/artworks/search?q=${term}&limit=8`);
    const json = await response.json();

    if(json && json.data) {
      return json.data.map((item: ArtworksListAPI) => {
        return {
          id: item.id,
          title: item.title,
        }
      })
    } else {
      return []
    }

  } catch (e) {
    throw(e);
  }
}
