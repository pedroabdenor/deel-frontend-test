import { useCallback, useEffect, useState } from 'react';
import Autocomplete from './components/AutoComplete/AutoComplete';
import { getArtworksList, getArtworksItem } from './api/ArtworksAPI';
import { ArtworksItem } from './types/ArtworksItem'
import './App.css'
import logo from './assets/logo.png';
import loadingGif from './assets/loading.gif'

function App() {
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);
  const [imageLoading, setImageLoading] = useState(true);
  const [optionSelected, setOptionSelected] = useState<ArtworksItem>({ title: '', image_id: '' });

  useEffect(() => {
    setImageLoading(true)
    resetOptionSelected()
  }, [options])

  const getAutoCompleteOptions = useCallback(async(term : string) => {
    resetOptionSelected()
    if(term) {
      setLoading(true)
      const options = await getArtworksList(term)
      setOptions(options)
      setLoading(false)
    }
  }, [])

  const getAutoCompleteSelectedOption = async(id : number) => {
    const item = await getArtworksItem(id)
    setOptionSelected(item)
  }

  const resetOptionSelected = () => {
    setOptionSelected({ title: '', image_id: '' })
  }

  const onImageLoad = () => {
    setImageLoading(false)
  }

  return (
    <div className="App">
      <header className='App-header'>
        <img src={logo} alt="" className='App-logo' />
        <h1>Auto-complete component for deel frontend test using  Art Institute of Chicago API</h1>
      </header>

      <Autocomplete placeholder="Search by Artworks" options={options} getOptions={getAutoCompleteOptions} loading={loading} onSelect={getAutoCompleteSelectedOption} />

      {optionSelected.image_id && (
        <div className="App-content">
          <div className="App-content-images">
            {imageLoading && <img src={loadingGif} alt="loading..." className='loading' /> }
            <img src={`https://www.artic.edu/iiif/2/${optionSelected.image_id}/full/1686,/0/default.jpg`} alt={optionSelected.title} onLoad={onImageLoad} className={!imageLoading ? 'displayImage' : ''} />
          </div>
          <h3>{optionSelected.title}</h3>
        </div>
      )}

    </div>
  );
}

export default App;
