import React, { useState, useEffect } from 'react';
import './AutoComplete.css';
import useDebounce from '../../utils/useDebounce'
import loadingImage from '../../assets/loading.gif'

interface AutocompleteOptions {
  id: number
  title: string
}

interface AutocompleteProps {
  options: AutocompleteOptions[]
  getOptions: (option: string) => void
  onSelect: (option: number) => void
  placeholder: string
  loading: boolean
}

const Autocomplete: React.FC<AutocompleteProps> = ({ options, getOptions, onSelect, placeholder, loading }) => {
  const [inputValue, setInputValue] = useState('');
  const [filteredOptions, setFilteredOptions] = useState<AutocompleteOptions[]>([]);
  const debouncedSearchTerm = useDebounce<string>(inputValue, 500)

  useEffect(() => {
    if (debouncedSearchTerm.length !== 0) {
      getOptions(debouncedSearchTerm.trim())
    }
  }, [debouncedSearchTerm, getOptions]);

  useEffect(() => {
    if (inputValue.length > 0 && options.length > 0) {
      const searchTerm = inputValue.toLowerCase();

      let filtered = options.filter(option =>
        option.title.toLowerCase().includes(searchTerm)
      );

      let sortOptions = filtered.sort((a, b) =>
        a.title.toLocaleLowerCase().indexOf(searchTerm) - b.title.toLocaleLowerCase().indexOf(searchTerm)
      );

      setFilteredOptions(sortOptions);
    } else {
      setFilteredOptions([]);
    }
  }, [options, inputValue]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value)
  };

  const buildHighlightTerm = (term: String, highlight: String) => {
    const parts = term.split(new RegExp(`(${highlight})`, "gi"));
    return parts.map((part, i) => (
      part.toLowerCase() === highlight.toLowerCase() ? <strong key={i}>{part}</strong> : part
    ))
  }

  const handleSelectOption = (id: number) => {
    onSelect(id)
    setFilteredOptions([]);
    setInputValue('')
  };

  const onClickInput = () => {
    getOptions(inputValue.trim())
  }

  return (
    <div className="autocomplete">
      <div className="autocomplete-bar">
        <input
          type="text"
          value={inputValue}
          onClick={onClickInput}
          onChange={handleInputChange}
          placeholder={placeholder}
          className='autocomplete-bar-input'
          tabIndex={0}
        />
        <button className="autocomplete-bar-button" type="submit" tabIndex={100}>
          <span>&#9906;</span>
        </button>
      </div>


      <div className="autocomplete-results">

        {loading && (
          <img src={loadingImage} alt="loading" className='autocomplete-results-loading'/>
        )}

        {!loading && filteredOptions.length > 0 && (
          <ul className="autocomplete-results-options">
            {filteredOptions.map((option, index) => (
              <li key={option.id} tabIndex={index+1} onClick={() => handleSelectOption(option.id)}>
                <span>{buildHighlightTerm(option.title, inputValue)}</span>
              </li>
            ))}
          </ul>
        )}

      </div>


    </div>
  );
};

export default Autocomplete;
